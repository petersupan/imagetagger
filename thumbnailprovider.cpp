#include "thumbnailprovider.h"
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QImageReader>
#include "metadata.h"


ThumbnailProvider::ThumbnailProvider(QObject *parent) : QQuickImageProvider(QQuickImageProvider::Image), QObject(parent)
{

}

void ThumbnailProvider::getAllFilenames()
{
	QDir myDir(_pathname);
	QDirIterator it(_pathname, QStringList() << "*.jpg", QDir::Files, QDirIterator::Subdirectories);
	while (it.hasNext()) {
		QString fn = it.next();
		_allFilenames.push_back(fn);
		qDebug() << fn;
	}
}

QStringList getKeywordList(std::string keywords)
{
	QString kwS = QString(keywords.c_str());
	QRegExp separator("(,|;|\\s)");
	QStringList _kwList = kwS.split(separator);
	QStringList kwList;
	for(int i = 0; i < _kwList.size(); i++) {
		if (_kwList[i] != "" && _kwList[i] != " ") {
			kwList.append(_kwList[i]);
		}
	}
	return kwList;
}

void ThumbnailProvider::createThumbnails()
{
	for(int i = 0; i < _allFilenames.size(); i++) {
		QString currentFn = _allFilenames[i];
		QImage img = QImage(currentFn);
		QImageReader reader(currentFn);
		QSize size = reader.size();
		reader.setScaledSize(QSize(256, 256));
		QImage tmp = reader.read();
		QStringList list = reader.textKeys();
		for(int i = 0; i < list.size(); i++) {
			qDebug() << list[i];
		}

		std::string keywordString =  MetaData::getKeywords(currentFn.toStdString());
		QStringList keywordList = getKeywordList(keywordString);

		//MetaData::printMetaData(currentFn.toStdString());

		QImage thmb = img.scaled(256, 256, Qt::KeepAspectRatio, Qt::SmoothTransformation);
		_allThumbImages.push_back(thmb);
		Thumbnail* nail = new Thumbnail();
		QString strippedFn = currentFn.mid(_pathname.size());
		nail->filename = strippedFn;
		nail->id = i;
		_allThumbs.push_back(nail);
		QStringList tags;
		//tags.push_back(QString("Tag_")+QString::number(i));
		_allKeywords.push_back(keywordList);
	}
	QStringList kwList = findAllOccurringKeywords();
	_keywordFilter.clear();
	for(int i = 0; i < kwList.size(); i++) {
		Keyword* kw = new Keyword(kwList[i]);
		_keywordFilter.push_back(kw);
	}

	emit thumbsChanged();
}

void ThumbnailProvider::setPathname(const QString &pathname)
{
	if (pathname == _pathname) {
		return;
	}

	_pathname = pathname;
	_pathname = _pathname.remove("file:///");
	getAllFilenames();
	createThumbnails();

	emit pathnameChanged();
}

const QString& ThumbnailProvider::pathname()
{
	return _pathname;
}

QImage ThumbnailProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
	int _id = id.toInt();
	if (_id < _allThumbImages.size()) {
		return _allThumbImages[_id];
	}
	return QImage();
}

QList<QObject*> ThumbnailProvider::thumbsAsObjectList() const
{
	QList<QObject*> ret;
	foreach (QObject* cur, _allThumbs) {
		ret << cur;
	}
	return ret;
}

QList<QObject*> ThumbnailProvider::filteredThumbs()  const
{
	QStringList keywords;
	for(int i = 0; i < _keywordFilter.size(); i++) {
		if(_keywordFilter[i]->checked) {
			keywords.push_back(_keywordFilter[i]->name);
		}
	}


	QList<QObject*> ret;
	for(int i = 0; i < _allThumbs.size(); i++) {
		QStringList currentKw = _allKeywords[i];
		// is irgendeines der keywords drinnen in derlist
		for(int k = 0; k < keywords.size(); k++) {
			if (currentKw.indexOf(keywords[k]) != -1) {
				ret << _allThumbs[i];
				break;
			}
		}
		if(_showThumbsWithNoKeyword && currentKw.size() == 0) {
			ret << _allThumbs[i];
		}
	}
	return ret;
}

QStringList ThumbnailProvider::findAllOccurringKeywords() const
{
	QStringList occurringKeywords;
	for (int i = 0; i < _allKeywords.size(); i++) {
		for(int j = 0; j < _allKeywords[i].size(); j++) {
			QString current = _allKeywords[i][j];
			if (occurringKeywords.indexOf(current) == -1) {
				occurringKeywords.push_back(current);
			}
		}
	}
	return occurringKeywords;
}

QStringList ThumbnailProvider::getTags(int idx) const
{
	if (idx < _allKeywords.size()) {
		return _allKeywords[idx];
	}
	return QStringList();
}

QString concatKeywordList(QStringList l)
{
	QString output;
	for(auto i : l) {
		if (i == " " || i == "") {
			continue;
		}
		output.append(i);
		output.append(";");
	}
	return output;
}

void ThumbnailProvider::addTag(QString tag, int idx)
{
	if (idx < _allKeywords.size()) {
		// only add tag if it is not already there
		bool found = false;
		for(int i = 0; i< _allKeywords[idx].size(); i++) {
			if (_allKeywords[idx][i] == tag) {
				found = true;
			}
		}
		if (!found) {
			_allKeywords[idx].push_back(tag);
			QString conCatedKeywords = concatKeywordList(_allKeywords[idx]);
			MetaData::writeKeywords(_allFilenames[idx].toStdString(), conCatedKeywords.toStdString());
		}
	}

	bool found = false;
	// add keyword to filter, if not already there
	for(int i = 0; i < _keywordFilter.size(); i++) {
		if (_keywordFilter[i]->name == tag) {
			found = true;
		}
	}
	if (!found) {
		Keyword* kw = new Keyword(tag);
		_keywordFilter.push_back(kw);
	}
}

void ThumbnailProvider::removeTag(QString tag, int idx)
{
	if (idx < _allKeywords.size()) {

		// can only remove tag if it is there
		int delidx = _allKeywords[idx].indexOf(tag);
		if (delidx != -1) {
			_allKeywords[idx].removeAt(delidx);
			/// @warning UNTESTED!
			QString conCatedKeywords = concatKeywordList(_allKeywords[idx]);
			MetaData::writeKeywords(_allFilenames[idx].toStdString(), conCatedKeywords.toStdString());
		}
	}
}


QList<QObject*> ThumbnailProvider::occurringKeywords() const
{
	QList<QObject*> ret;
	foreach (QObject* cur, _keywordFilter) {
		ret << cur;
	}
	return ret;
}

void ThumbnailProvider::setKeywordChecked(int idx, bool checked)
{
	if(idx < _keywordFilter.size() ) {
		_keywordFilter[idx]->checked = checked;
	}
	emit thumbsChanged();
}

void ThumbnailProvider::setNoKeywordChecked(bool checked)
{
	_showThumbsWithNoKeyword = checked;
	emit thumbsChanged();
}

void ThumbnailProvider::storeFilteredToFolder(QString folder) const
{
	folder = folder.remove("file:///");

	QList<QObject*> filtered = filteredThumbs();
	for(int i = 0; i < filtered.size(); i++) {
		Thumbnail* thmb = (Thumbnail*) filtered[i];
		QString fn = thmb->filename;
		QString alternateFn = _allFilenames[thmb->id];
		if (fn != alternateFn) {
			qDebug() << "verdammte Scheisse";
		}
		qDebug() << fn << "   " << folder;
		QString destFn = folder + fn;
		qDebug() << "Copying " << alternateFn << " to " << destFn;

		QFileInfo info(destFn);
		QDir fileDirectory = info.absoluteDir();
		if (!fileDirectory.exists()){
		  fileDirectory.mkdir(".");
		}
		qDebug() << "The fileDirectory is " << fileDirectory;

		QFile::copy(alternateFn, destFn);
	}
}

QString ThumbnailProvider::getFullFilename(int idx) const
{
	if (idx < _allFilenames.size()) {
		return QString("file:///") + _allFilenames[idx];
	}
}

