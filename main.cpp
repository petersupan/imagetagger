#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "thumbnailprovider.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QGuiApplication app(argc, argv);

	ThumbnailProvider prov;

	QQmlApplicationEngine engine;
	engine.load(QUrl(QLatin1String("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	engine.addImageProvider(QLatin1String("Thumbnails"), &prov);
	engine.rootContext()->setContextProperty("thumbnailProvider", &prov);


	return app.exec();
}
