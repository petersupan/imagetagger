import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

ApplicationWindow {
	visible: true
	width: 1280
	height: 1024
	title: qsTr("Hello World")

	Thumbsview {
		id: thumbsView
		anchors.top: button.bottom
		anchors.left: parent.left
		anchors.right: keyShorts.left
		anchors.bottom: parent.bottom
		focus: true
	}

	Button {
		id: button
		text: "Choose folder"
		onClicked: fileDialog.visible = true;
	}

	Button {
		id: filterbutton
		text: "Filter"
		anchors.left: button.right
		onClicked: {
			filterSettings.visible = !filterSettings.visible;
			if (!filterSettings.visible) {
				console.log("model wird refreshed");
				thumbsView.refreshModel();
			}
		}
	}

	Button {
		id: saveFiltered
		anchors.left: filterbutton.right
		text: "Save Filtered"
		onClicked: saveFolderDialog.visible = true;
	}

	Keywordtab {
		id: keyShorts
		anchors.top: button.bottom
		anchors.right: parent.right
		width: 200
		anchors.bottom: info.top
	}

	InfoTab {
		id: info
		anchors.bottom: parent.bottom
		anchors.left: thumbsView.right
		anchors.right: parent.right
		height: 200
	}

	FilterSettings {
		id: filterSettings
		visible: false
		anchors.top: filterbutton.bottom
		anchors.left: filterbutton.left
		anchors.bottom: parent.verticalCenter
	}

	FullScreenView {
		id: fullScreenView
		visible: false
		clip: true

		anchors.fill: thumbsView

		Keys.onPressed: {
			if (event.key === Qt.Key_Escape) {
				console.log("Escape pressed, visible false")

				fullScreenView.focus = false;
				thumbsView.forceActiveFocus();
				fullScreenView.visible = false;
				console.log("thumbsview focus: " + thumbsView.activeFocus);
			} else if (event.key === Qt.Key_Right) {
				thumbsView.changeCurrentIndex(1);
				var ffname = thumbsView.getCurrentIndexFullFilename();
				source = ffname;
			} else if (event.key === Qt.Key_Left) {
				thumbsView.changeCurrentIndex(-1);
				source = thumbsView.getCurrentIndexFullFilename();
			} else {
				var selectedList = [thumbsView.currentIndex];
				keyShorts.keyPressed(event, selectedList);
			}
		}

		Keys.onReleased: {
			if (event.key !== Qt.Key_Escape) {
				keyShorts.keyReleased(event);
			}
		}
	}

	FileDialog {
		id: fileDialog
		selectFolder: true
		title: "Please choose a Folder"
		folder: shortcuts.pictures
		onAccepted: {
			console.log("You chose: " + fileDialog.fileUrls)
			thumbnailProvider.pathname = fileDialog.folder;
		}
		onRejected: {
			console.log("Canceled")
		}
	}

	FileDialog {
		id: saveFolderDialog
		selectFolder: true
		title: "Please choose a Folder to store images in"
		folder: shortcuts.pictures
		onAccepted: {
			console.log("You chose: " + saveFolderDialog.fileUrls)
			thumbnailProvider.storeFilteredToFolder(saveFolderDialog.folder);
		}
		onRejected: {
			console.log("Canceled")
		}
	}


}
