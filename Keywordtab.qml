import QtQuick 2.7
import QtQuick.Controls 1.4

Rectangle {
	id: keyShorts


	property bool delPressed: false

	function keyPressed(event, selectedList) {
		// delete key wie einen modifier (strg oder so) verwenden
		if (event.key === Qt.Key_Delete) {
			delPressed = true;
			return;
		}
		if (event.key === Qt.Key_Control) {
			return;
		}

		// alle durchgehen und schaun ob das Tag gmeint ist
		for (var i = 0; i < repeater.count; i++) {
			// der shortcut Buchstabe das dazugehörige Keyword
			var shortcutLetter = repeater.itemAt(i).getShortcutText();
			var keywordText = repeater.itemAt(i).getTagText();
			// wenn das aktuelle gemeint ist
			if (event.text === shortcutLetter) {
				// wenn del gedrückt ist löschen, sonst hinzufügen
				if (!delPressed) {
					for(var j = 0; j < selectedList.length; j++) {
						var idx = selectedList[j];
						thumbnailProvider.addTag(keywordText, thumbsView.model[idx].id);
					}
				} else {
					for(var k = 0; k < selectedList.length; k++) {
						var idx = selectedList[k];
						thumbnailProvider.removeTag(keywordText, thumbsView.model[idx].id);
					}
				}
			}
		}
		// updaten
		info.showNew(thumbsView.currentIndex);//thumbsView.model[thumbsView.currentIndex].id);
	}


	function keyReleased(event) {
		if (event.key === Qt.Key_Delete) {
			delPressed = false;
		}
	}

	Column {
		Repeater {
			id: repeater
			model: 26
			property int currentFocus : 0
			Rectangle {
				height: 30
				width: 200
				color: "white"
				function getShortcutText() {
					return shortcut.text;
				}
				function getTagText() {
					return tag.text;
				}

				Row {
					id: row
					TextInput {
						id: shortcut
						selectByMouse: true
						overwriteMode: true
						width: 50
						height: 30
						text: "X"
						activeFocusOnTab: true
						onFocusChanged: {
							if (focus == true) {
								selectAll();
							}
						}
					}
					TextInput {
						id: tag
						selectByMouse: true
						overwriteMode: true
						width: 80
						height: 30
						text: "Tag"
						activeFocusOnTab: true
						onFocusChanged: {
							if (focus == true) {
								selectAll();
							}
						}
					}
					Button {
						id: tagButton
						text: tag.text
					}
				}
			}
		}
	}
}

