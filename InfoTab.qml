import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

Rectangle {
	id: info

	Text {
		id: infoText
		anchors.bottom: parent.bottom
		text: "nix"
	}

	function showNew(idx) {
		infoText.text = thumbsView.model[idx].filename + "\n " + thumbsView.model[idx].id
		var tagList = thumbnailProvider.getTags(thumbsView.model[idx].id)
		tagListView.model = tagList;
	}

	Component {
		id: tagListDelegate
		Rectangle {
			color: {
//					if (ListView.isCurrentItem)
//						"#DDFFDD"
//					else
				if (index % 2 == 0)
					"#FFFFFF"
				else
					"#CCCCCC"
			}
//				border.width: {
//					if (ListView.isCurrentItem)
//						1
//					else
//						0
//				}

			width: tagListView.width
			height: deleteTagButton.height+2
			Text {
				text: modelData
				anchors.right: deleteTagButton.left
				anchors.left: parent.left
			}
			Button {
				id: deleteTagButton
				text: "<b>X</b>"
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
				onClicked: {
					thumbnailProvider.removeTag(modelData, thumbsView.model[thumbsView.currentIndex].id)
					info.showNew(thumbsView.currentIndex);//thumbsView.model[thumbsView.currentIndex].id);
				}
			}
		}
	}

	ListView {
		id: tagListView
		anchors.fill: parent
		delegate: tagListDelegate
	}


}
