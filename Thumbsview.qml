import QtQuick 2.0

Item {
	property alias grid: thumbsView
	property alias model: thumbsView.model
	property alias currentIndex: thumbsView.currentIndex

	function getCurrentIndexFullFilename() {
		return thumbsView.getCurrentIndexFullFilename();
	}

	function changeCurrentIndex(change) {
		thumbsView.changeCurrentIndex(change);
	}

	function refreshModel() {
		thumbsView.refreshModel();
	}

	onFocusChanged: thumbsView.forceActiveFocus()

	Component {
		id: gridDelegate
		Item {
			width: 256
			height: 256

			Image {
				id: img
				source: "image://Thumbnails/" + model.modelData.id;
			}

			Text {
				anchors.bottom: parent.bottom
				text: model.modelData.filename + "\n " + model.modelData.id
			}

			Keys.onPressed: {
				if (event.key === Qt.Key_Space || event.key === Qt.Key_Tab) {
					event.accepted = true;
					GridView.view.moveCurrentIndexRight()
				} else if (event.key === Qt.Key_Return) {
					showFullScreenView();
				}
			}

			function showFullScreenView() {
				fullScreenView.source = thumbnailProvider.getFullFilename(model.modelData.id);
				fullScreenView.visible = true;
				fullScreenView.focus = true;
			}

			Rectangle {
				color: "transparent"
				border.color: model.modelData.selected ? "#00ff08" : "#000000"

				anchors.fill: parent
				border.width: 2
			}

			MouseArea {
				anchors.fill: parent
				onClicked:  {

					parent.GridView.view.currentIndex = index;
					parent.GridView.view.focus = true;
				}

				onDoubleClicked: showFullScreenView();
			}
		}
	}

	GridView {
		id: thumbsView
		anchors.fill: parent

		keyNavigationWraps: true
		focus: true;
		clip: true;
		cellWidth: 256; cellHeight: 256
		delegate: gridDelegate
		highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
		model: thumbnailProvider.thumbs

		property bool addToSelected: false
		property var selectedIndices : []

		function refreshModel() {
			model = thumbnailProvider.filteredThumbs();
		}

		function getCurrentIndexFullFilename() {
			var ids = model[currentIndex].id;
			var ffname = thumbnailProvider.getFullFilename(ids);
			return ffname;
		}

		function changeCurrentIndex(change) {
			var newIndex = currentIndex + change;
			if (newIndex > 0 && newIndex < model.length) {
				currentIndex = newIndex;
			}
		}

		Keys.onPressed: {
			if (event.key === Qt.Key_Control) {
				addToSelected = true;
			}
			keyShorts.keyPressed(event, selectedIndices);
		}

		Keys.onReleased: {
			if (event.key === Qt.Key_Control) {
				addToSelected = false;
			}
			keyShorts.keyReleased(event);
		}

		interactive: true
		onCurrentIndexChanged: {
			// if strg is not pressed, delete array before adding
			if (!addToSelected) {
				for(var i = 0; i < selectedIndices.length; i++) {
					model[selectedIndices[i]].selected = false;
				}
				selectedIndices.length = 0;
			}
			model[currentIndex].selected = true;
			selectedIndices.push( currentIndex);

			info.showNew(currentIndex)
			console.log("current index changed to " + currentIndex)
		}
	}
}
