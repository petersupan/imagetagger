import QtQuick 2.7

Rectangle {
	id: photoFrame

	property alias source: image.source;

	Image {
		id: image
		smooth: true
		mipmap: true
		x: photoFrame.x
		y: photoFrame.y
		width: photoFrame.width
		height: photoFrame.height
		fillMode: Image.PreserveAspectFit
	}

	MouseArea {
		id: dragArea
		preventStealing: true
		hoverEnabled: true
		anchors.fill: parent
		drag.target: image
		drag.axis: Drag.XAndYAxis

		onClicked: {
			photoFrame.forceActiveFocus();
		}

		onWheel: {
			var normMousex = mouseX - dragArea.width/2;
			var normMousey = mouseY - dragArea.height/2;
			var vecToCenterX = (image.x - normMousex)*0.1;
			var vecToCenterY = (image.y - normMousey)*0.1;
			if (wheel.angleDelta.y > 0.0) {
				image.scale *= 1.1;
				image.x += vecToCenterX;
				image.y += vecToCenterY;
			} else {
				image.scale *= 0.9;
				image.x -= vecToCenterX;
				image.y -= vecToCenterY;
			}
		}
	}
}


