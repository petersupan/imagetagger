#ifndef THUMBNAILPROVIDER_H
#define THUMBNAILPROVIDER_H

#include <QObject>
#include <QQuickImageProvider>
#include <QImage>
#include <QString>
#include <QVector>
#include <QMediaMetaData>

class Thumbnail : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString filename MEMBER filename CONSTANT)
	Q_PROPERTY(qint32 id MEMBER id CONSTANT)
	Q_PROPERTY(bool selected READ selected WRITE setSelected NOTIFY selectedChanged)

public:
	bool selected() const {return _selected;}
	void setSelected(bool sel)
	{
		if (sel != _selected) {
			_selected = sel;
			emit selectedChanged();
		}
	}

	QString filename;
	qint32 id;
	bool _selected = false;

signals:
	void selectedChanged();
};

class Keyword : public QObject {
	Q_OBJECT
	Q_PROPERTY(QString name MEMBER name CONSTANT)
	Q_PROPERTY(bool checked MEMBER checked CONSTANT)
public:
	Keyword(QString n) : name(n), checked(true)
	{}
	QString name;
	bool checked;
};

class ThumbnailProvider : public QObject, public  QQuickImageProvider
{
	Q_OBJECT
	Q_PROPERTY(QString pathname READ pathname WRITE setPathname NOTIFY pathnameChanged)
	Q_PROPERTY(QList<QObject*> thumbs READ thumbsAsObjectList NOTIFY thumbsChanged)
	//Q_PROPERTY(QList<QObject*> keywords READ keywordsAsObjectList NOTIFY keywordsChanged)
public:
	ThumbnailProvider(QObject* parent = nullptr);

	void setPathname(const QString& pathname);
	const QString& pathname();

	QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);

	QList<QObject*> thumbsAsObjectList() const;

	Q_INVOKABLE QList<QObject*> filteredThumbs() const;

	Q_INVOKABLE void storeFilteredToFolder(QString folder) const;

	Q_INVOKABLE QList<QObject*> occurringKeywords() const;
	Q_INVOKABLE void setKeywordChecked(int idx, bool checked);
	Q_INVOKABLE void setNoKeywordChecked( bool checked);

	Q_INVOKABLE QStringList getTags(int idx) const;
	Q_INVOKABLE void addTag(QString tag, int idx);
	Q_INVOKABLE void removeTag(QString tag, int idx);
	Q_INVOKABLE QString getFullFilename(int idx) const;

signals:
	void pathnameChanged();
	void thumbsChanged();
	void keywordsChanged();

private:
	QString _pathname;
	QVector<QString> _allFilenames;
	QVector<QImage> _allThumbImages;
	QVector<Thumbnail*> _allThumbs;
	QVector<QStringList> _allKeywords;

	QVector<Keyword*> _keywordFilter;
	bool _showThumbsWithNoKeyword;

	void getAllFilenames();
	void createThumbnails();
	QStringList findAllOccurringKeywords() const;

};

#endif // THUMBNAILPROVIDER_H
