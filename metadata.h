#ifndef METADATA_H
#define METADATA_H

#include <string>
#include <vector>

class MetaData
{
public:
	MetaData();

	std::vector<std::string> getMetaData();
	static std::string getKeywords(std::string filename);
	static void writeKeywords(
			const std::string &filename, const std::string &keywords);


	static void printMetaData(std::string filename);


private:
	std::vector<std::string> _keywords;
};

#endif // METADATA_H
