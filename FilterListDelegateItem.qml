import QtQuick 2.7
import QtQuick.Controls 1.4

Rectangle {
	id: filterListDelegateItem
	property alias filterBox: _filterBox
	property alias tagText: _tagText


	width: 200
	height: _filterBox.height+2

	CheckBox {
		id: _filterBox
	}

	Text {
		id: _tagText
		anchors.left: _filterBox.right
	}
}
