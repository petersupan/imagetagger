#include "metadata.h"

#include <exiv2/exiv2.hpp>
#include <assert.h>
#include <QDebug>
#include <sstream>

#include <codecvt>
#include <cassert>
#include <locale>

MetaData::MetaData()
{

}

std::string reverseByteOrder(std::string& input) {
	assert(input.size() %2 == 0);
	std::string output = input;

	for(int i = 0; i < input.size(); i+=2) {
		output[i] = input[i+1];
		output[i+1] = input[i];
	}
	return output;
}

std::string stringFromMetaString(std::string& _input) {
	std::string output;
	QString input(_input.c_str());
	QStringList l = input.split(" ");
	for(auto i :l) {
		output.push_back(i.toInt());
	}
	return output;
}

std::string stringToMetaString(const std::string& _input) {
	std::string output;
	for(int i = 0; i < _input.size(); i++) {
		std::string metaString = std::to_string(_input[i]);
		output += metaString;
		output += " 0 ";
	}
	output += "0 0";
	return output;
}

void MetaData::writeKeywords(const std::string &filename, const std::string &keywords)
{
	try {
		Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filename);
		assert (image.get() != 0);
		image->readMetadata();
		Exiv2::ExifData &exifData = image->exifData();
		std::string metaStr = stringToMetaString(keywords);
		Exiv2::Exifdatum& tag = exifData["Exif.Image.XPKeywords"];
		//exifData["Exif.Image.XPKeywords"] = metaStr;
		tag.setValue(metaStr);
		image->setExifData(exifData);
		image->writeMetadata();
	} catch (Exiv2::AnyError& e) {
		qDebug() << "Exiv2 exception  in writeKeywords'" << e.what() << "'\n";
	}

}

std::string MetaData::getKeywords(std::string filename) {
	std::string keywords;
	Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filename);
	assert (image.get() != 0);
	try {
		image->readMetadata();
		Exiv2::ExifData &exifData = image->exifData();
		if (exifData.empty()) {
			std::string error(filename);
			error += ": No Exif data found in the file";
			throw Exiv2::Error(1, error);
		}
		Exiv2::Exifdatum& xpKeywords = exifData["Exif.Image.XPKeywords"];
		const Exiv2::Value& v = xpKeywords.value();
		std::string tmp = stringFromMetaString(v.toString());
		std::u16string u16string = (char16_t*)(tmp.c_str());
		std::string u8_conv = std::wstring_convert<
				std::codecvt_utf8_utf16<ushort>, ushort>{}.to_bytes((ushort*)(tmp.c_str()));
		keywords += u8_conv;
		keywords += " ";
		// Das würd auch alles funktionieren
//		QString test = QString::fromWCharArray((wchar_t*)(tmp.c_str()));
//		QString test2 = QString::fromStdU16String(u16string);
//		QString test3 = QString::fromWCharArray((wchar_t*)(tmp.c_str()));

		Exiv2::Exifdatum& userComment = exifData["Exif.Photo.UserComment"];
		qDebug() << "UserComment: " << userComment.value().toString().c_str();
		Exiv2::Exifdatum& description = exifData["Exif.Image.ImageDescription"];
		qDebug() << "Description: " << description.value().toString().c_str();
	}	catch (Exiv2::Error& e) {
		qDebug() << "Caught Exiv2 exception '" << e.what() << "'\n";
	}


	try {
		Exiv2::IptcData &iptcData = image->iptcData();
		if (iptcData.empty()) {
			std::string error(filename);
			error += ": No IPTC data found in the file";
			throw Exiv2::Error(1, error);
		}
	} catch(Exiv2::AnyError& e) {
		qDebug() << "Caught Exiv2 exception '" << e.what() << "'\n";
	}

	return keywords;
}

void getMetaData(std::string filename) {
	Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filename);
	assert (image.get() != 0);
	image->readMetadata();
	Exiv2::ExifData &exifData = image->exifData();

}

void MetaData::printMetaData(std::string filename) {
	try {
		//std::string fn = "C:/Users/admin/Pictures/TestPictures/IMG_00000010.jpg";
		qDebug() << "NOW COMES IMAGE " << QString(filename.c_str());
		Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filename);
		assert(image.get() != 0);
		image->readMetadata();
		Exiv2::ExifData &exifData = image->exifData();
		if (exifData.empty()) {
			std::string error(filename);
			error += ": No Exif data found in the file";
			throw Exiv2::Error(1, error);
		}
		Exiv2::ExifData::const_iterator end = exifData.end();
		for (Exiv2::ExifData::const_iterator i = exifData.begin(); i != end; ++i) {
			const char* tn = i->typeName();
			std::stringstream ss;
			ss << std::setw(44) << std::setfill(' ') << std::left
					  << i->key() << " "
					  << "0x" << std::setw(4) << std::setfill('0') << std::right
					  << std::hex << i->tag() << " "
					  << std::setw(9) << std::setfill(' ') << std::left
					  << (tn ? tn : "Unknown") << " "
					  << std::dec << std::setw(3)
					  << std::setfill(' ') << std::right
					  << i->count() << "  "
					  << std::dec << i->value()
					  << "\n";
			qDebug() << ss.str().c_str();
		}
	}
	//catch (std::exception& e) {
	//catch (Exiv2::AnyError& e) {
	catch (Exiv2::Error& e) {
		qDebug() << "Caught Exiv2 exception '" << e.what() << "'\n";
	}

	try {
		Exiv2::Image::AutoPtr image = Exiv2::ImageFactory::open(filename);
		assert (image.get() != 0);
		image->readMetadata();
		Exiv2::IptcData &iptcData = image->iptcData();
		if (iptcData.empty()) {
			std::string error(filename);
			error += ": No IPTC data found in the file";
			throw Exiv2::Error(1, error);
		}
		Exiv2::IptcData::iterator end = iptcData.end();
		for (Exiv2::IptcData::iterator md = iptcData.begin(); md != end; ++md) {
			std::stringstream ss;
			ss << std::setw(44) << std::setfill(' ') << std::left
					  << md->key() << " "
					  << "0x" << std::setw(4) << std::setfill('0') << std::right
					  << std::hex << md->tag() << " "
					  << std::setw(9) << std::setfill(' ') << std::left
					  << md->typeName() << " "
					  << std::dec << std::setw(3)
					  << std::setfill(' ') << std::right
					  << md->count() << "  "
					  << std::dec << md->value()
					  << std::endl;
			qDebug() << ss.str().c_str();
		}
	}
	catch (Exiv2::AnyError& e) {
		qDebug() << "Caught Exiv2 exception '" << e.what() << "'\n";
	}
}


