import QtQuick 2.7

Rectangle {
	id: filterSettings
	visible: false
	anchors.top: filterbutton.bottom
	anchors.left: filterbutton.left
	anchors.bottom: parent.verticalCenter

	FilterListDelegateItem {
		id: noTagFilter
		anchors.top: parent.top
		anchors.left: parent.left

		filterBox.checked: true
		tagText.text: "no Tag"

		filterBox.onCheckedChanged: {
			thumbnailProvider.setNoKeywordChecked(filterBox.checked);
			filterSelectionView.showNew();
		}
	}

	Component {
		id: filterListDelegate
		FilterListDelegateItem {
			color: {
				if (index % 2 == 0)
					"#FFFFFF"
				else
					"#CCCCCC"
			}
			filterBox.checked: modelData.checked
			filterBox.onCheckedChanged: {
				thumbnailProvider.setKeywordChecked(index, filterBox.checked);
				filterSelectionView.showNew();
			}
			tagText.text: modelData.name
		}
	}

	ListView {
		id: filterSelectionView
		anchors.top: noTagFilter.bottom
		anchors.bottom: parent.bottom

		delegate: filterListDelegate
		visible: true

		function showNew() {
			model = thumbnailProvider.occurringKeywords();
		}

		onVisibleChanged: {
			if (visible === true) {
				showNew();
			}
		}
	}
}
